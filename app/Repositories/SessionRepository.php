<?php

namespace App\Repositories;

use App\Exceptions\Database\DatabaseException;
 use App\Resources\User\Progress\HistoryItemResource;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use PDO;

class SessionRepository
{
    /** @var PDO */
    private $pdo;

    public function __construct()
    {
        $this->pdo = DB::connection()->getPdo();
    }

    /**
     * Get the history of the last N sessions
     * Contains {score, date} fields
     *
     * @param int $userId
     * @param int $limit
     *
     * @return array
     * @throws DatabaseException
     */
    public function getHistoryByUserId(int $userId, int $limit = 12): array
    {
        $cacheKey = sprintf('%s_%s_%s', __METHOD__ , $userId, $limit);

        return Cache::rememberForever($cacheKey, function () use ($userId, $limit) {
            $statement = $this->pdo->prepare(
                'SELECT score, timestamp FROM sessions WHERE user_id = ? ORDER BY timestamp, id DESC LIMIT ?'
            );
            $statement->execute([$userId, $limit]);

            if (!$statement) {
                throw new DatabaseException('The database server cannot successfully execute the statement');
            }

            return $statement->fetchAll(PDO::FETCH_CLASS, HistoryItemResource::class);
        });
    }

    /**
     * Get category names for the recent session of the given user
     *
     * @param int $userId
     *
     * @return array
     * @throws DatabaseException
     */
    public function getRecentTrainedCategoriesByUserId(int $userId): array
    {
        $cacheKey = sprintf('%s_%s', __METHOD__, $userId);

        return Cache::rememberForever($cacheKey, function () use ($userId) {
            $statement = $this->pdo->prepare(
                'SELECT DISTINCT c_t.name FROM
                (SELECT id FROM sessions WHERE user_id=? ORDER BY timestamp, id DESC LIMIT 1) sid_t
                    JOIN session_exercises se_t on sid_t.id = se_t.session_id
                    JOIN exercises e_t ON se_t.exercise_id = e_t.id
                    JOIN categories c_t ON e_t.category_id = c_t.id'
            );
            $statement->execute([$userId]);

            if (!$statement) {
                throw new DatabaseException('The database server cannot successfully execute the statement');
            }

            return $statement->fetchAll(PDO::FETCH_COLUMN, 0);
        });
    }
}
