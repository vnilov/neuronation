<?php

namespace App\Resources\User\Progress;

class HistoryItemResource
{
    public $score;
    public $date;
    private $timestamp;

    public function __construct()
    {
        $this->date = strtotime($this->timestamp);
    }
}
