<?php


namespace App\Responses;


use Illuminate\Http\JsonResponse;

class ErrorResponse extends JsonResponse
{
    public function __construct(string $errorMessage, $headers = [], $options = 0)
    {
        parent::__construct([
            'error' => $errorMessage
        ], 500, $headers, $options);
    }
}
