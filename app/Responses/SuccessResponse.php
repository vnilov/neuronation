<?php


namespace App\Responses;


use Illuminate\Http\JsonResponse;

class SuccessResponse extends JsonResponse
{
    public function __construct($data = null, $headers = [], $options = 0)
    {
        parent::__construct($data, 200, $headers, $options);
    }
}
