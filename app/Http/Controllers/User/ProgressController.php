<?php

namespace App\Http\Controllers\User;

use App\Exceptions\Database\DatabaseException;
use App\Repositories\SessionRepository;
use App\Responses\ErrorResponse;
use App\Responses\SuccessResponse;
use Illuminate\Http\JsonResponse;

class ProgressController
{
    /** @var */
    private $sessionRepository;

    public function __construct(SessionRepository $sessionRepository)
    {
        $this->sessionRepository = $sessionRepository;
    }

    /**
     * @param int $userId
     * @return ErrorResponse|SuccessResponse
     */
    public function history(int $userId): JsonResponse
    {
        try {
            $data = [
                'history' => $this->sessionRepository->getHistoryByUserId($userId)
            ];
        } catch (DatabaseException $databaseException) {
            return new ErrorResponse($databaseException->getMessage());
        }

        return new SuccessResponse($data);
    }

    /**
     * @param int $userId
     * @return ErrorResponse|SuccessResponse
     */
    public function recentCategories(int $userId): JsonResponse
    {
        try {
            $data = $this->sessionRepository->getRecentTrainedCategoriesByUserId($userId);
        } catch (DatabaseException $databaseException) {
            return new ErrorResponse($databaseException->getMessage());
        }

        return new SuccessResponse($data);
    }
}
