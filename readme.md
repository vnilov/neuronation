# Database Challenge
![DB Scheme](https://gitlab.com/vnilov/neuronation/raw/master/resources/readme/db_scheme.png)
#### Description
- First of all, I decided to compound _sessions_ and _scores_ tables. Score, as I understood it correctly, is a part of
 each session. This improvement allow us to get score for the session without any joins.
- Also after this change I can create a relation between _users_ and _sessions_ table. I changed _0 or more_ relation
to __1 or more__, because as I checked on the web site and mobile app you can't do any exercises before login.
- I created _many to many_ relation between _exercises_ and _sessions_ tables,  because that will allow us to get
exercises of each session faster.

I hadn't had any rules for the field's type and length, so I created it as **VARCHAR(255)** and **INT(11)**.

# Code Challenge
Here I decided to use Lumen Framework as helper for fast development.
#### Description
##### To implementing these two API endpoints I created:
- **app/Repositories/SessionRepository.php** class, to get session results from DB. I used permanent cache there, because
in the real word I'd create additional events to clean it.
-- In case of getting user history there is a simple request to the _sessions_ table.
-- In case of getting categories' name there is a little bit bigger request:
![Query explain](https://gitlab.com/vnilov/neuronation/raw/master/resources/readme/explain.png)
- **app/Resources/User/Progress/HistoryItemResource.php** class to keep and modify history result
- **app/Http/Controllers/User/ProgressController.php** controller class to handle incoming requests
- **app/Responses/ErrorResponse.php** and **app/Responses/SuccessResponse.php** classes to handle API responses.    


##### Routes(routes/web.php):
I put all the routes under **/users/{userId}/progress/** namespace:
- **/users/{userId}/progress/history**
- **/users/{userId}/progress/recent/categories**
That would be more clear to use on the frontend layer


##### To testing the application I added:
- **tests/Repositories/RepositoryTest.php** tests for SessionRepository class
- **tests/End2End/ProgressControllerTest.php** end-to-end test for controller.
![Tests](https://gitlab.com/vnilov/neuronation/raw/master/resources/readme/tests.png)


# PostScript
- I used raw SQL fro queries and PDO driver instead of using Lumen models and Eloquent as you asked me.
- All tables were created by migrations, you can find it here: database/migrations
- Also, I created a small helper to filling the database and test my SQL queries: **tests/Helpers/DatabaseBuilder.php**.

    

