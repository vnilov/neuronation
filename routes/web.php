<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['prefix' => 'users'], function () use ($router) {
    $router->group(['prefix' => '{userId}', 'namespace' => 'User'], function () use ($router) {
        $router->group(['prefix' => 'progress'], function () use ($router) {
            $router->get('history', [
                'as' => 'user.progress.history', 'uses' => 'ProgressController@history'
            ]);
            $router->group(['prefix' => 'recent'], function () use ($router) {
                $router->get('categories', [
                    'as' => 'user.progress.recent.categories', 'uses' => 'ProgressController@recentCategories'
                ]);
            });
        });
    });
});
