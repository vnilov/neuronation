<?php

namespace Helpers;

use Faker\Factory;
use Faker\Generator;
use Illuminate\Support\Facades\DB;
use PDO;
use PDOException;

class DatabaseBuilder
{
    /** @var PDO */
    private $pdo;
    /** @var Generator */
    private $faker;

    public function __construct()
    {
        $this->pdo   = DB::connection()->getPdo();
        $this->faker = Factory::create();
    }

    public function build(int $factor = 1)
    {
        $this->addCategories(1, 10 * $factor);
        $this->addCourses(1, 30 * $factor);
        $this->addUsers(1, 100 * $factor);
        $this->addExercises(1, 400 * $factor);
        $this->addSessions(1, 500 * $factor);
    }

    /**
     * Fill categories table by fake data
     * @param int $min
     * @param int $max
     */
    private function addCategories(int $min, int $max)
    {
        $count = rand($min, $max);
        for ($i = 0; $i < $count; $i++) {
            $statement = $this->pdo->prepare('INSERT INTO categories SET name = ?');
            $statement->execute([$this->faker->word]);
        }
    }

    /**
     * Fill courses table by fake data
     * @param int $min
     * @param int $max
     */
    private function addCourses(int $min, int $max)
    {
        $count = rand($min, $max);
        for ($i = 0; $i < $count; $i++) {
            $statement = $this->pdo->prepare('INSERT INTO courses SET name = ?');
            $statement->execute([$this->faker->word]);
        }
    }

    /**
     * Fill users table by fake data
     * @param int $min
     * @param int $max
     */
    private function addUsers(int $min, int $max)
    {
        $count = rand($min, $max);
        for ($i = 0; $i < $count; $i++) {
            $statement = $this->pdo->prepare(
                'INSERT INTO users (username, password, status) VALUES (?, ?, ?)'
            );
            $statement->execute([
                $this->faker->firstName,
                $this->faker->password,
                1
            ]);
        }
    }

    /**
     * Fill exercises table by fake data
     * @param int $min
     * @param int $max
     */
    private function addExercises(int $min, int $max)
    {
        $coursesIDs    = $this->pdo->query('SELECT id FROM courses')
            ->fetchAll(PDO::FETCH_COLUMN, 0);
        $categoriesIDs = $this->pdo->query('SELECT id FROM categories')
            ->fetchAll(PDO::FETCH_COLUMN, 0);

        $count = rand($min, $max);
        for ($i = 0; $i < $count; $i++) {
            $statement = $this->pdo->prepare(
                'INSERT INTO exercises (name, points, category_id, course_id) VALUES (?, ?, ?, ?)'
            );

            $statement->execute([
                $this->faker->word,
                $this->faker->numberBetween(),
                $categoriesIDs[array_rand($categoriesIDs)],
                $coursesIDs[array_rand($coursesIDs)]
            ]);
        }
    }

    /**
     * Fill sessions table by fake data
     * @param int $min
     * @param int $max
     */
    private function addSessions(int $min, int $max)
    {
        $count      = rand($min, $max);
        $added      = 0;
        $userOffset = 0;

        try {
            $this->pdo->beginTransaction();
            while ($count > $added) {
                // get value of the current user sessions
                $userSessions = rand(1, 500);
                // get the user id
                $userIdStatement = $this->pdo->prepare('SELECT id FROM users LIMIT 1 OFFSET ?');
                $userIdStatement->bindValue(1, $userOffset, PDO::PARAM_INT);
                $userIdStatement->execute();

                if ($userId = $userIdStatement->fetchColumn()) {
                    $userOffset++;
                    // add sessions for the current user
                    for ($i = 0; $i < $userSessions; $i++) {
                        $userScore        = rand(1, 10);
                        $sessionStatement = $this->pdo->prepare(
                            'INSERT INTO sessions (user_id, score, score_normalized) VALUES (?, ?, ?)'
                        );
                        $sessionStatement->execute([$userId, $userScore, $userScore]);

                        $sessionID = (int)$this->pdo->lastInsertId();
                        // get random 5 exercises
                        $exercisesIDs = $this->pdo->query(
                            'SELECT id FROM exercises 
                                JOIN (SELECT ROUND(RAND() * (SELECT MAX(id) FROM exercises)) as randomID) as random
                                    WHERE exercises.id > random.randomID LIMIT 5'
                        )->fetchAll(PDO::FETCH_COLUMN, 0);

                        if (count($exercisesIDs) > 0) {
                            $rowsSQL = [];
                            foreach ($exercisesIDs as $exercisesID) {
                                $rowsSQL[] = '(' . $sessionID . ',' . $exercisesID . ')';
                            }
                            $statement = 'INSERT INTO session_exercises (session_id, exercise_id) VALUES '
                                . implode(', ', $rowsSQL);
                            // add relation between exercises and session
                            $this->pdo->exec($statement);
                        }
                    }
                    $added += $userSessions;
                }
            }
            $this->pdo->commit();
        } catch (PDOException $e) {
            $this->pdo->rollBack();
        }
    }
}
