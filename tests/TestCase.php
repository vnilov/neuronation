<?php

use Helpers\DatabaseBuilder;
use Illuminate\Support\Facades\DB;

abstract class TestCase extends Laravel\Lumen\Testing\TestCase
{
    /** @var PDO */
    protected $pdo;
    /** @var int */
    protected $userId;

    public function setUp(): void
    {
        parent::setUp();
        $this->pdo = DB::connection()->getPdo();

        /** @var DatabaseBuilder $dbBuilder */
        $dbBuilder = app()->make(DatabaseBuilder::class);
        $dbBuilder->build();

        // get user
        $this->userId = $this->pdo->query('SELECT id FROM users limit 1')
            ->fetch(PDO::FETCH_COLUMN, 0);
    }

    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__.'/../bootstrap/app.php';
    }
}
