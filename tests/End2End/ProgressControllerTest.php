<?php

class ProgressControllerTest extends TestCase
{
    public function testHistory()
    {
        $path   = sprintf('/users/%s/progress/history/', $this->userId);

        $this->get($path);
        $responseArr = json_decode($this->response->getContent(), true);

        $this->assertResponseStatus(200);
        $this->assertJson($this->response->getContent());
        $this->assertArrayHasKey('history', $responseArr);
        $this->assertIsArray($responseArr['history']);
    }

    public function testRecentCategories()
    {
        $path   = sprintf('/users/%s/progress/recent/categories', $this->userId);

        $this->get($path);
        $responseArr = json_decode($this->response->getContent());

        $this->assertResponseStatus(200);
        $this->assertGreaterThan(0, count($responseArr));
    }
}
