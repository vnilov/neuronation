<?php

use App\Exceptions\Database\DatabaseException;
use App\Repositories\SessionRepository;
use App\Resources\User\Progress\HistoryItemResource;
use Laravel\Lumen\Testing\DatabaseMigrations;

class SessionRepositoryTest extends TestCase
{
    use DatabaseMigrations;

    /** @var SessionRepository */
    private $sessionRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->sessionRepository = app()->make(SessionRepository::class);
    }

    /**
     * @throws DatabaseException
     */
    public function testHistoryByUserId()
    {
        $lastSessions = $this->pdo->query(
            'SELECT score, timestamp FROM sessions WHERE user_id = ' . $this->userId . ' ORDER BY timestamp, id DESC LIMIT 12')
            ->fetchAll(PDO::FETCH_CLASS, HistoryItemResource::class);

        $data = $this->sessionRepository->getHistoryByUserId($this->userId);

        $this->assertEquals($lastSessions, $data);
    }

    /**
     * @throws DatabaseException
     */
    public function testRecentTrainedCategoriesTest()
    {
        $lastSessionId = $this->pdo->query(
            'SELECT id FROM sessions where user_id = ' . $this->userId . ' ORDER BY timestamp, id DESC LIMIT 1'
        )->fetch(PDO::FETCH_COLUMN, 0);
        $exercisesIds  = $this->pdo->query(
            'SELECT exercise_id FROM session_exercises WHERE session_id = ' . $lastSessionId
        )->fetchAll(PDO::FETCH_COLUMN, 0);

        $categoriesIds = $this->pdo->query(
            'SELECT DISTINCT category_id FROM exercises WHERE id IN (' . implode(', ', $exercisesIds) . ')'
        )->fetchAll(PDO::FETCH_COLUMN, 0);
        $categories    = $this->pdo->query(
            'SELECT name FROM categories WHERE id IN (' . implode(', ', $categoriesIds) . ')'
        )->fetchAll(PDO::FETCH_COLUMN, 0);

        $data = $this->sessionRepository->getRecentTrainedCategoriesByUserId($this->userId);

        $this->assertEquals($categories, $data);
    }
}
