<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /** @var PDO $pdo */
        $pdo = DB::connection()->getPdo();
        $pdo->exec('CREATE TABLE IF NOT EXISTS sessions (
            id               INT NOT NULL AUTO_INCREMENT,
            user_id          INT NOT NULL,
            score            INT DEFAULT 0,
            score_normalized INT DEFAULT 0,
            start_diff       INT DEFAULT 0,
            end_diff         INT DEFAULT 0,
            timestamp        TIMESTAMP DEFAULT CURRENT_TIMESTAMP,

            PRIMARY KEY (id),
            INDEX (user_id),

            FOREIGN KEY (user_id)
              REFERENCES users(id)
                ON DELETE CASCADE
        )');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sessions');
    }
}
