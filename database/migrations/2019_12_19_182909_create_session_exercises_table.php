<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateSessionExercisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /** @var PDO $pdo */
        $pdo = DB::connection()->getPdo();
        $pdo->exec('CREATE TABLE IF NOT EXISTS session_exercises (
            session_id  INT NOT NULL,
            exercise_id INT NOT NULL,

            PRIMARY KEY (session_id, exercise_id),
            
            FOREIGN KEY (session_id)
              REFERENCES sessions(id)
                ON DELETE CASCADE,              
            FOREIGN KEY (exercise_id)
              REFERENCES exercises(id)
                ON DELETE CASCADE
        )');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('session_exercises');
    }
}
