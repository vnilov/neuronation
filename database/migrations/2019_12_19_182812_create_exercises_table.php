<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateExercisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /** @var PDO $pdo */
        $pdo = DB::connection()->getPdo();
        $pdo->exec('CREATE TABLE IF NOT EXISTS exercises (
            id          INT NOT NULL AUTO_INCREMENT,
            course_id   INT NULL,
            category_id INT NOT NULL,
            name        VARCHAR(255) NOT NULL,
            points      INT NOT NULL,

            PRIMARY KEY (id),
            INDEX (course_id),
            INDEX (category_id),
            FOREIGN KEY (course_id)
              REFERENCES courses(id)
                ON DELETE CASCADE,
            FOREIGN KEY (category_id)
              REFERENCES categories(id)
                ON DELETE CASCADE
        )');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exercises');
    }
}
